import Vue from 'vue'
import App from './App.vue'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import Service from '../plugins/Service'
import 'material-icons/iconfont/material-icons.css';
import Storage from '../plugins/Storage'

Vue.use(Service)
Vue.use(Storage)
Vue.use(Vuesax, {
  // options here
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
